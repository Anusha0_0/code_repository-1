Write programs to print the following patterns using appropriate programming constructs.

1. @@@
   @@@
   @@@

2. *
   **
   ***

3. 1234
   1234
   1234
   1234

4. 1
   12
   123
   1234

5. 1
   22
   333
   4444
   55555

6. AAA
   BBB
   CCC

7. A 
   ABC
   ABCDE
   ABCDEFG

8. 0 
   12
   345
   6789

9. 1
   12
   123
   1234
   12345